import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

// const one: number = 1

async function bootstrap(): Promise<void> {
  // FIX-ME: make port num an env variable
  const port = 9000;
  const app = await NestFactory.create(AppModule);

  console.log( `server running on port= ${port}`);

  await app.listen(port);
}
bootstrap();

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ListingsModule } from './listings/listings.module';

@Module({

  controllers: [AppController],
  providers: [AppService],
  imports: [ListingsModule],
})
export class AppModule {}

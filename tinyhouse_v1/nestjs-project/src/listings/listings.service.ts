import { Injectable } from '@nestjs/common';

import { Listing } from './listing.model';
// contains inmemory data
import { listings } from './listings';

@Injectable()
export class ListingsService {
    private listingsData: Listing[];
    constructor() {
        this.listingsData = listings;
    }

    getListings(): Listing[] {
        return this.listingsData;
    }

    deleteListing( id: string): void {
       this.listingsData =  this.listingsData.filter(listing => listing.id !==  id );
        // for (let i = 0; i < this.listingsData.length; i++) {
        //     if (this.listingsData[i].id === id ) {
        //          deletedData: Listing = this.listingsData.splice(i, 1);
        //         return deletedData;
        //     }
        // }
    }
}

import { Controller, Get,  Delete, Param } from '@nestjs/common';
import { ListingsService } from './listings.service';
import { Listing } from './listing.model';

@Controller('listings')
export class ListingsController {
    constructor( private listingsService: ListingsService) {}

    @Get()
    getAllListings(): Listing[] {
        return this.listingsService.getListings();
    }

    @Delete(':id')
    deleteListing(@Param('id') id: string): void {
        this.listingsService.deleteListing(id );
    }
}
